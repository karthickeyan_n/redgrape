import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import './index.css'
// import 'bootstrap/dist/css/bootstrap.min.css';
import RedGrapeContainer from './RedGrapeContainer/RedGrapeContainer';

const RedGrape = () => {

    return(
        <>
        <RedGrapeContainer/>
        </>
    )
    
}

ReactDOM.render(<RedGrape/>, document.getElementById('redgrape'));

export default RedGrape;