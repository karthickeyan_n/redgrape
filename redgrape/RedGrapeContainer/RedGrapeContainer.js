import React, { Component } from "react";
import styled from "@emotion/styled";
import axios from "axios";
import { RadioButton } from "primereact/radiobutton";
import { _get } from "lodash";
import * as images from "../Assets/Images";
import {Container, Col, Row } from "react-bootstrap";

const Logo = styled.div`
  padding: 20px 10px;
  float: left;
`;

const HeaderSlogan = styled.div`
  padding: 25px 15px 0 0;
  text-align: right;
  font-family: open-sans, sans-serif !important;
`;
const Slogan = styled.div`
  color: #000000;
  line-height: 26px;
  padding: 15px 0 0 0;
  font-weight: 700;
  font-size: 22px;
`;

const Image = styled.div`
max-width: 100%;
height: auto;
`;
const Tabs = styled.div`
  margin-top: 10px;
  width: 100%;
`;
const CaseOptions = styled.div`
  display: inline-block;
  position: relative;
  margin-top: 10px;
  font-size: 1.6em;
  margin-right: 10px;
  width: 41%;
  float: left;
  left: 170px;
  cursor: default;
`;

const Buttonstyle = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  margin-right: 5px;
  border-radius: 0;
  background: ${(props) =>
    (props.active === "Reds" && "#d7182a") ||
    (props.active === "Whites" && "#cc903c") ||
    (props.active === "Mixed" && "#447838")};
  border-color: ${(props) =>
    (props.active === "Reds" && "#d7182a") ||
    (props.active === "Whites" && "#cc903c") ||
    (props.active === "Mixed" && "#447838")};
  width: 100px;
  height: 45px;
  color: ${(props) => (props.active ? "#fff" : "#000")};
  border: 1px solid #848484;
  font-size: 1.125rem;
  cursor: pointer;
  border-left: ${(props) => props.active && "8px solid transparent"};
  border-right: ${(props) => props.active && "8px solid transparent"};
  outline: none;
`;

const MainContent = styled.div`
  float: left;
  text-align: left;
  width: 65%;
  color: #333;
`;

const Title = styled.h1`
 font-size: 18px;
 font-weight: 700;
 color: #d7182a;
 padding: 8px 0 8px 0;
}
 `;
const Paragraph = styled.p`
  font-size: 14px;
  padding-bottom: 12px;
`;

const Bold = styled.p`
  font-weight: bold;
  display: inline;
`;
const OfferContent = styled.div`
  margin-top: 15px;
  text-align: left;
`;

const SectionHeader = styled.div`
  margin: 20px 0;
  padding: 3px 0 1px;
  position: relative;
  text-align: left;
  color: #333;
  float: left;
  width: 65%;
`;

const Step = styled.div`
 width: 80px;
 color: #999;
 letter-spacing: .8px;
 position: absolute;
 top:0;
 background-color: #fff;
 font-size: 18.5px;
 font-weight: 700;
}
 `;
const SectionContent = styled.div`
  color: #000;
  font-size: 18.5px;
  text-align: left;
  margin: 0 0 0 80;
`;
const Border = styled.div`
  border-bottom: 1px solid #999;
`;

const Radio = styled.div`
  padding: 5 0;
`;

const Margin = styled.div`
  margin: 10;
`;
const Alert = styled.div`
  color: #b94a48;
  background-color: #f2dede;
  border-color: #eed3d7;
  clear: both;
  padding: 8px 35px 8px 14px;
  margin-bottom: 20px;
  font-size: 0.9rem;
  text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
  border: 1px solid #fbeed5;
  border-radius: 4px;
  cursor: pointer;
`;
class RedGrapeContainer extends Component {
  state = {
    buttonValue: "Reds",
    offersData: [],
    caseType: "Reds Mixed Case",
    inputValue: "",
    error: false,
    ZipcodeData: {},
    ZipApiSuccess: false,
    ZipApiFailed: false,
    offerApi: false,
  };


  const [banner, setBanner] = useState(true);

  

  componentDidMount() {
    axios
      .get(`https://www.wsjwine.com/api/offer/0033008`)
      .then((res) => {
        const response = _.get(res.data.response, "mainItems", "");
        this.setState({ offersData: response });
      })
      .catch((error) => {
        this.setState({ offerApi: true });
      });
  }

  RadioButtonHandler = (e, value) => {
    const stateObj = {};
    if (value === "Reds Mixed Case") {
      (stateObj.caseType = value), (stateObj.buttonValue = "Reds");
    } else if (value === "Whites Mixed Case") {
      (stateObj.caseType = value), (stateObj.buttonValue = "Whites");
    } else {
      (stateObj.caseType = value), (stateObj.buttonValue = "Mixed");
    }
    this.setState({
      caseType: stateObj.caseType,
      buttonValue: stateObj.buttonValue,
    });
  };

  buttonHandler = (value) => {
    const stateObj = {};
    if (value === "Reds")
      (stateObj.caseType = "Reds Mixed Case"), (stateObj.buttonValue = value);
    else if (value === "Whites") {
      (stateObj.caseType = "Whites Mixed Case"), (stateObj.buttonValue = value);
    } else {
      (stateObj.caseType = "Mixed Case"), (stateObj.buttonValue = value);
    }
    this.setState({
      caseType: stateObj.caseType,
      buttonValue: stateObj.buttonValue,
    });
  };

  eventChecker = (value) => {
    if (value.length === 5) {
      axios
        .get(`https://www.wsjwine.com/api/address/zipcode/` + value)
        .then((res) => {
          const ZipcodeData = _.get(res.data, "response", "");
          this.setState({ ZipcodeData: ZipcodeData, ZipApiSuccess: true });
          console.log("res of zipcode", res);
        })
        .catch((error) => {
          this.setState({ ZipApiFailed: true, ZipApiSuccess: false });
          console.log("error from applicationCache", error);
        });
      console.log("the.length is ", value.length);
    }
  };
  inputChangeHandler = (e) => {
    if (!isNaN(e.target.value))
      this.setState({ inputValue: e.target.value, error: false }, () =>
        this.eventChecker(this.state.inputValue)
      );
    else {
      this.setState({ error: true });
    }
  };

  render() {
    const {
      offersData = [],
      caseType,
      error,
      inputValue,
      ZipcodeData,
      ZipApiSuccess,
      ZipApiFailed,
    } = this.state;
    let image = <img src={images.redBottles} alt="red bottles" />;
    if (this.state.buttonValue === "Whites") {
      image = <img src={images.whiteBottles} alt="red bottles" />;
    } else if (this.state.buttonValue === "Mixed") {
      image = <img src={images.mixedBottles} alt="mixed bottles" />;
    }
    return (
      <>
      <Container>
        <Row>
        <Col>
        <Logo>
          <a href="https://www.wsjwine.com/jsp/homepage.jsp">
            <img
              src={images.wsgLogo}
              alt="WSJwine from The Wall Street Journal"
            />
          </a>
        </Logo>
        </Col>
        <HeaderSlogan>
          <Slogan>{this.props.slogan} </Slogan>
        </HeaderSlogan>
        <Row>
        <Col >
          <Image>
        {image}
        </Image>
        </Col>
        </Row>
        <Tabs>
          <CaseOptions>{this.props.caseOptions}</CaseOptions>
          {this.props.buttonContent.map((buttonContent, index) => (
            <Buttonstyle
              key={index}
              active={
                this.state.buttonValue === buttonContent
                  ? this.state.buttonValue
                  : false
              }
              onClick={() => this.buttonHandler(buttonContent)}
            >
              {buttonContent}{" "}
            </Buttonstyle>
          ))}
        </Tabs>
        <MainContent>
          <Title>{this.props.title}</Title>
          <Paragraph>{this.props.paragraph1}</Paragraph>
          <Paragraph>
            {" "}
            <Bold>{this.props.paragraphBold2}</Bold>
            {this.props.paragraph2}
          </Paragraph>
          <Paragraph>
            {" "}
            <Bold>{this.props.paragraphBold3}</Bold>
            {this.props.paragraph3}
          </Paragraph>
        </MainContent>
        <SectionHeader>
          <Step>STEP 1:</Step>
          <SectionContent>{this.props.step1}</SectionContent>
          <Border />
          <Paragraph>{this.props.paragraph4}</Paragraph>
          {offersData.map((offersData) =>
            offersData.product.skus.map((skus) => {
              return (
                <Radio key={skus.id}>
                  {" "}
                  <RadioButton
                    type="radio"
                    value={caseType}
                    checked={caseType === skus.caseType}
                    onChange={(e) => this.RadioButtonHandler(e, skus.caseType)}
                  />
                  <label
                    htmlFor=""
                    style={{
                      position: "relative",
                      bottom: "15px",
                      left: "27px",
                    }}
                  >
                    {offersData.product.name} and {skus.numberOfBottles} Bottles
                    for JUST ${skus.salePrice}
                  </label>
                </Radio>
              );
            })
          )}
        </SectionHeader>
        <SectionHeader>
          <Step>STEP 2:</Step>
          <SectionContent>{this.props.step2}</SectionContent>
          <Border />
          <Margin>
            <input
              name="Zipcode"
              type="text"
              maxlength="5"
              onChange={(e) => {
                this.inputChangeHandler(e);
              }}
              value={this.state.inputValue}
            />
            <span style={{ margin: "0 0 0 20" }}>
              {ZipApiSuccess
                ? `${ZipcodeData && ZipcodeData.city}, ${
                    ZipcodeData && ZipcodeData.stateName
                  }`
                : this.props.Enter}
            </span>
          </Margin>
          {(ZipApiFailed && !ZipApiSuccess) || error ? (
            <Alert> {this.props.valid}</Alert>
          ) : (
            ""
          )}
        </SectionHeader>
        </Row>
        </Container>
      </>
    );
  }
}

RedGrapeContainer.defaultProps = {
  slogan: "Special Welcome Offer",
  caseOptions: "Your Case Options",
  buttonContent: ["Reds", "Whites", "Mixed"],
  title: "These are the dozen wines you need to taste …",
  paragraph1:
    "Many of our Top 12 wines have won major awards. Others have been recorded as a favorite by thousands of wine fans online. You'll uncork them all for ONLY $69.99 as your introduction to the WSJwine Discovery Club.",
  paragraph2:
    "you'll also enjoy two bottles of a stunningly rich, 98-point Super Tuscan—made in the traditional appassimento method—plus a pair of fine, stemless glasses (worth $64.97).",
  paragraphBold2: "It gets better … ",
  paragraph3:
    "Discovery Club members earn credits for free bottles, upgrades to a 1.5-liter magnum and a luxury bottle (worth $40+), plus exclusive offers throughout the year.",
  paragraphBold3: "The rewards continue … ",
  step1: "Which Case Would You Like?",
  paragraph4:
    "Whatever you choose, we'll add in 2 BONUS, 98-point Super Tuscans and 2 stemless glasses. The complete package—worth over $260—is yours for ONLY $69.99 (plus $19.99 shipping & applicable tax).",
  step2: "Where Would You Like Your Wine Delivered?",
  Enter: "Enter ZIP to populate City and State",
  valid: "Please enter a valid zip code and try again.",
};
export default RedGrapeContainer;
