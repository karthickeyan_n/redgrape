

export const  redBottles = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_reds_69_save190_2BonusGoverno2Glasses_desktop.jpg";


export const whiteBottles = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_whites_69_save190_2BonusGoverno2Glasses_desktop.jpg ";

export const mixedBottles = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_mix_69_save190_2BonusGoverno2Glasses_desktop.jpg";

export const wsgLogo = "https://www.wsjwine.com/images/us/common/recr/wsj_logo_rebrand.png";

export const redBottlesSmall = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_reds_69_save190_2BonusGoverno2Glasses_mobile.jpg";

export const whiteBottlesSmall = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_whites_69_save190_2BonusGoverno2Glasses_mobile.jpg";

export const mixedBottlesSmall = "https://www.wsjwine.com/images/us/wsj/offers/temp6/fy20/header/top12Q3_mix_69_save190_2BonusGoverno2Glasses_mobile.jpg";

